class AddActableTypeAndActableIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :actable_id, :integer
    add_index :users, :actable_id
    add_column :users, :actable_type, :string
  end
end
