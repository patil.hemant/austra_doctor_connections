class RemoveUserIdFromBusinesses < ActiveRecord::Migration[6.0]
  def change
    remove_column :businesses, :user_id
  end
end
