class AddBusinessIdToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :business_id, :integer
    add_index :posts, :business_id
  end
end
