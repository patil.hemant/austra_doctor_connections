class AddExperienceInYearsToJobs < ActiveRecord::Migration[6.0]
  def change
    add_column :jobs, :experience_in_years, :string
  end
end
