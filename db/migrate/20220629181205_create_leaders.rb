class CreateLeaders < ActiveRecord::Migration[6.0]
  def change
    create_table :leaders do |t|
      t.string :headline
      t.string :content
      t.integer :user_id
      t.integer :business_id
      t.boolean :visible

      t.timestamps
    end
    add_index :leaders, :user_id
    add_index :leaders, :business_id
  end
end
