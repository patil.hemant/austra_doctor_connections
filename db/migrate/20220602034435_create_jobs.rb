class CreateJobs < ActiveRecord::Migration[6.0]
  def change
    create_table :jobs do |t|
      t.string :title
      t.integer :user_id
      t.string :posting_for
      t.integer :speciality_id
      t.string :country_id
      t.string :state_id
      t.string :city_id
      t.string :employment_type
      t.string :description
      t.string :qualification
      t.string :about_organization
      t.string :website_link
      t.string :job_type

      t.timestamps
    end
    add_index :jobs, :user_id
    add_index :jobs, :speciality_id
  end
end
