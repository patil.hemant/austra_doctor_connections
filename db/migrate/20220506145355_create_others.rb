class CreateOthers < ActiveRecord::Migration[6.0]
  def change
    create_table :others do |t|
      t.string :organization_id

      t.timestamps
    end
  end
end
