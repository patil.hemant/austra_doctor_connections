class CreateEventUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :event_users do |t|
      t.integer :user_id
      t.integer :event_id
      t.boolean :speaker, default: :false

      t.timestamps
    end
    add_index :event_users, :user_id
    add_index :event_users, :event_id
  end
end
