class CreateBusinessUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :business_users do |t|
      t.integer :business_id
      t.integer :user_id

      t.timestamps
    end
    add_index :business_users, :business_id
    add_index :business_users, :user_id
  end
end
