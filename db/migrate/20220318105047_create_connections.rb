class CreateConnections < ActiveRecord::Migration[6.0]
  def change
    create_table :connections do |t|
      t.integer :user_a_id
      t.integer :user_b_id

      t.timestamps
    end
    add_index :connections, :user_a_id
    add_index :connections, :user_b_id
  end
end
