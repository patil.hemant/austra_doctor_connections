class CreatePolls < ActiveRecord::Migration[6.0]
  def change
    create_table :polls do |t|
      t.string :question
      t.string :duration
      t.integer :post_id

      t.timestamps
    end
    add_index :polls, :post_id
  end
end
