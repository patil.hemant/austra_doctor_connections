class AddSignedUpAsAdminToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :signed_up_as_admin, :boolean, default: false
  end
end
