class CreateDomainNames < ActiveRecord::Migration[6.0]
  def change
    create_table :domain_names do |t|
      t.string :name
      t.integer :speciality_id

      t.timestamps
    end
    add_index :domain_names, :speciality_id
  end
end
