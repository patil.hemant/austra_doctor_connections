class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.integer :business_id
      t.integer :category_id
      t.string :title
      t.text :description

      t.timestamps
    end
    add_index :courses, :business_id
    add_index :courses, :category_id
  end
end
