class CreateUserSavedJobs < ActiveRecord::Migration[6.0]
  def change
    create_table :user_saved_jobs do |t|
      t.integer :user_id
      t.integer :job_id

      t.timestamps
    end
    add_index :user_saved_jobs, :user_id
    add_index :user_saved_jobs, :job_id
  end
end
