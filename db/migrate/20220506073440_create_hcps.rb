class CreateHcps < ActiveRecord::Migration[6.0]
  def change
    create_table :hcps do |t|
      t.string :existing_email
      t.integer :speciality_id
      t.string :service_interested_in

      t.timestamps
    end
    add_index :hcps, :speciality_id
  end
end
