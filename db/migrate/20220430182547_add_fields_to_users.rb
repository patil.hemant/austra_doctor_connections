class AddFieldsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :mobile_no, :string
    add_column :users, :country_id, :integer
    add_column :users, :verified, :boolean, default: :false

    add_index :users, :country_id
  end
end
