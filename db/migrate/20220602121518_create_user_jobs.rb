class CreateUserJobs < ActiveRecord::Migration[6.0]
  def change
    create_table :user_jobs do |t|
      t.integer :user_id
      t.integer :job_id
      t.string :status

      t.timestamps
    end
    add_index :user_jobs, :user_id
    add_index :user_jobs, :job_id
  end
end
