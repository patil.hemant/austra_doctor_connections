class AddVotesCountToPolls < ActiveRecord::Migration[6.0]
  def change
    add_column :polls, :votes_count, :integer
  end
end
