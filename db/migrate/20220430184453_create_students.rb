class CreateStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :students do |t|
      t.integer :speciality_id
      t.string :educational_institution_website
      t.date :course_completion_date
      t.string :service_interested_in

      t.timestamps
    end
    add_index :students, :speciality_id
  end
end
