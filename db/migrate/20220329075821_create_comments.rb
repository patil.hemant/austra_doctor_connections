class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.integer :commentable_id
      t.string :commentable_type
      t.string :content
      t.integer :user_id
      t.integer :parent_id

      t.timestamps
    end
    add_index :comments, :commentable_id
    add_index :comments, :user_id
    add_index :comments, :commentable_type
    add_index :comments, :parent_id
  end
end
