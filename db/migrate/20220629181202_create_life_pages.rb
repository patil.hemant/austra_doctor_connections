class CreateLifePages < ActiveRecord::Migration[6.0]
  def change
    create_table :life_pages do |t|
      t.string :page_name
      t.integer :business_id
      t.string :image_url_link
      t.string :pixel_tracker_url

      t.timestamps
    end
    add_index :life_pages, :business_id
  end
end
