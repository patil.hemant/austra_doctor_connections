class CreateResumes < ActiveRecord::Migration[6.0]
  def change
    create_table :resumes do |t|
      t.integer :user_id
      t.timestamps
    end

    add_index :resumes, :user_id
  end
end