class CreateSpotlights < ActiveRecord::Migration[6.0]
  def change
    create_table :spotlights do |t|
      t.string :title
      t.text :description
      t.integer :life_page_id
      t.boolean :visible
      t.string :optional_caption

      t.timestamps
    end
    add_index :spotlights, :life_page_id
  end
end
