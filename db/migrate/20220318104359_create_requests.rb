class CreateRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :requests do |t|
      t.integer :requestor_id
      t.integer :receiver_id

      t.timestamps
    end
    add_index :requests, :requestor_id
    add_index :requests, :receiver_id
  end
end
