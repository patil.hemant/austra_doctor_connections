class AddAnswerToPollAnswer < ActiveRecord::Migration[6.0]
  def change
    add_column :poll_answers, :answer, :string
  end
end
