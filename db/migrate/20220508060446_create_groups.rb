class CreateGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :groups do |t|
      t.string :name
      t.string :description
      t.integer :user_id
      t.boolean :visible
      t.boolean :can_invite_connections

      t.timestamps
    end
    add_index :groups, :user_id
  end
end
