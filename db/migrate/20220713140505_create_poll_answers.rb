class CreatePollAnswers < ActiveRecord::Migration[6.0]
  def change
    create_table :poll_answers do |t|
      t.integer :poll_id

      t.timestamps
    end
    add_index :poll_answers, :poll_id
  end
end
