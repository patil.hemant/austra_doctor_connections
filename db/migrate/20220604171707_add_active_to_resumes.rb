class AddActiveToResumes < ActiveRecord::Migration[6.0]
  def change
    add_column :resumes, :active, :boolean
  end
end
