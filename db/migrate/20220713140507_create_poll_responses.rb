class CreatePollResponses < ActiveRecord::Migration[6.0]
  def change
    create_table :poll_responses do |t|
      t.integer :poll_answer_id
      t.integer :user_id

      t.timestamps
    end
    add_index :poll_responses, :poll_answer_id
    add_index :poll_responses, :user_id
  end
end
