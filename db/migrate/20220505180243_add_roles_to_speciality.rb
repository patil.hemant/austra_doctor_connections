class AddRolesToSpeciality < ActiveRecord::Migration[6.0]
  def change
    add_column :specialities, :roles, :text, array: true, default: []
  end
end
