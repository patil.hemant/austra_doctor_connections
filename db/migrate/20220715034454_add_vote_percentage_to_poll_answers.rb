class AddVotePercentageToPollAnswers < ActiveRecord::Migration[6.0]
  def change
    add_column :poll_answers, :vote_percentage, :integer
  end
end
