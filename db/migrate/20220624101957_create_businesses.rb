class CreateBusinesses < ActiveRecord::Migration[6.0]
  def change
    create_table :businesses do |t|
      t.string :business_type
      t.integer :user_id
      t.string :name
      t.string :austra_public_url
      t.string :button_name
      t.string :website
      t.string :tag_line
      t.string :description
      t.string :phone
      t.string :year_founded
      t.string :location
      t.string :hashtag1
      t.string :hashtag2
      t.string :hashtag3
      t.string :highlight_article1
      t.string :highlight_article2
      t.string :highlight_article3

      t.timestamps
    end
    add_index :businesses, :user_id
  end
end
