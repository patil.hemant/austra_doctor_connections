class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :organizable_type
      t.integer :organizable_id
      t.string :event_type
      t.string :name
      t.string :timezone
      t.datetime :start_date_time
      t.datetime :end_date_time
      t.string :link
      t.string :description
      t.integer :user_id

      t.timestamps
    end
    add_index :events, :organizable_id
    add_index :events, :user_id
  end
end
