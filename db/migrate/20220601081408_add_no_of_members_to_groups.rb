class AddNoOfMembersToGroups < ActiveRecord::Migration[6.0]
  def change
    add_column :groups, :no_of_members, :integer, default: 0
  end
end
