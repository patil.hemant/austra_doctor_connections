class CreateDoctors < ActiveRecord::Migration[6.0]
  def change
    create_table :doctors do |t|
      t.string :existing_email
      t.integer :speciality_id
      t.string :service_interested_in

      t.timestamps
    end
    add_index :doctors, :speciality_id
  end
end
