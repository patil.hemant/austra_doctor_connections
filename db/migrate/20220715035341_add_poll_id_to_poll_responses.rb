class AddPollIdToPollResponses < ActiveRecord::Migration[6.0]
  def change
    add_column :poll_responses, :poll_id, :integer
    add_index :poll_responses, :poll_id
  end
end
