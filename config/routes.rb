Rails.application.routes.draw do
  get 'domain_names/index'
  get 'home/index'
  root 'home#index'
  # devise_for :users
  devise_for :users, :controllers => {:registrations => "devise/registrations"}
  resources :requests
  get '/connections/my_connections', to: 'connections#my_connections'
  get '/jobs/my_posted_jobs', to: 'jobs#my_posted_jobs'
  get '/jobs/my_applied_jobs', to: 'jobs#my_applied_jobs'
  get '/jobs/saved_jobs', to: 'jobs#saved_jobs'
  get '/jobs/states', to: 'jobs#states'
  get '/jobs/cities', to: 'jobs#cities'
  resources :connections
  resources :posts do
    resources :comments
    resources :likes
  end
  resources :comments do
    resources :likes
  end
  resources :user_groups, only: [:create, :destroy, :update]
  resources :user_jobs, only: [:create, :destroy, :update]
  resources :user_saved_jobs, only: [:create, :destroy, :update]
  resources :jobs
  resources :resumes
  resources :likes
  resources :groups
  resources :specialities, only: [:index]
  resources :domain_names, only: [:index]
  resources :poll_responses, only: [:create, :destroy]
  resources :registration_steps
  resources :events
  resources :businesses
  resources :courses
  resources :categories
  get '/get_users' => 'events#get_users'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
