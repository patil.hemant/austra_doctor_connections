class Student < ApplicationRecord
	acts_as :user

	# attr_accessor :mobile_no, :govt_id_proof, :country_id

	# validates :mobile_no, presence: :true

	has_one_attached :college_id
end
