class Comment < ApplicationRecord
	belongs_to :commentable, polymorphic: true
	belongs_to :parent, class_name: 'Comment', optional: true
	has_many :replies, class_name: 'Comment', foreign_key: :parent_id, dependent: :destroy
	has_many :likes, as: :likeable
	# has_many :comments, as: :commentable

	scope :parent_comments, -> { where(parent_id: nil) }
end
