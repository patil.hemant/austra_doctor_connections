class Job < ApplicationRecord
    validates :title, :description, presence: :true
    belongs_to :creator, class_name: 'User', foreign_key: :user_id

    has_many :user_jobs
    has_many :users, through: :user_jobs

    def location
        [city_id, state_id, country_id].compact.join(', ').titleize
    end
end
