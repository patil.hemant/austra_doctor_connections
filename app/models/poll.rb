class Poll < ApplicationRecord
    belongs_to :post
    has_many :poll_answers
    has_many :poll_responses
	accepts_nested_attributes_for :poll_answers, :reject_if => lambda { |a| a[:answer].blank? }, :allow_destroy => true
end
