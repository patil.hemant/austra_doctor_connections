# app/models/event.rb

# frozen_string_literal: true

class Event < ApplicationRecord
  validates :name, :description, presence: true
  
  TIME_ZONES = ['(UTC-12:00) International Date Line West', '(UTC-11:00) Midway Island, Samoa', '(UTC-10:00) Hawaii',
                '(UTC-09:30) Marquesas Islands', '(UTC-09:00) Aleutian Islands', '(UTC-08:00) Alaska', '(UTC-08:00) Pitcairn Islands', '(UTC-07:00) Pacific Time (US and Canada), Tijuana', '(UTC-07:00) Arizona', '(UTC-06:00) Mountain Time (US and Canada)', '(UTC-06:00) Chihuahua, La Paz, Mazatlan', '(UTC-06:00) Saskatchewan', '(UTC-06:00) Central America', '(UTC-05:00) Central Time (US and Canada)', '(UTC-05:00) Guadalajara, Mexico City, Monterrey', '(UTC-05:00) Bogota, Lima, Quito', '(UTC-04:00) Eastern Time (US and Canada)', '(UTC-04:00) Indiana (East)', '(UTC-04:00) Caracas, La Paz', '(UTC-04:00) Santiago', '(UTC-03:00) Atlantic Time (Canada)', '(UTC-03:00) Brasilia', '(UTC-03:00) Buenos Aires, Georgetown', '(UTC-02:30) Newfoundland and Labrador', '(UTC-02:00) Greenland', '(UTC-02:00) Mid-Atlantic', '(UTC-01:00) Cape Verde Islands', '(UTC) Azores', '(UTC) Coordinated Universal Time', '(UTC) Reykjavik', '(UTC+01:00) Dublin, Edinburgh, Lisbon, London', '(UTC+01:00) Casablanca, Monrovia', '(UTC+01:00) West Central Africa', '(UTC+02:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague', '(UTC+02:00) Sarajevo, Skopje, Warsaw, Zagreb', '(UTC+02:00) Brussels, Copenhagen, Madrid, Paris', '(UTC+02:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', '(UTC+02:00) Cairo', '(UTC+02:00) Harare, Pretoria', '(UTC+03:00) Bucharest', '(UTC+03:00) Helsinki, Kiev, Riga, Sofia, Tallinn, Vilnius', '(UTC+03:00) Athens, Istanbul, Minsk', '(UTC+03:00) Jerusalem', '(UTC+03:00) Moscow, St. Petersburg, Volgograd', '(UTC+03:00) Kuwait, Riyadh', '(UTC+03:00) Nairobi', '(UTC+03:00) Baghdad', '(UTC+04:00) Abu Dhabi, Muscat', '(UTC+04:00) Baku, Tbilisi, Yerevan', '(UTC+04:30) Tehran', '(UTC+04:30) Kabul', '(UTC+05:00) Ekaterinburg', '(UTC+05:00) Islamabad, Karachi, Tashkent', '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', '(UTC+05:30) Sri Jayawardenepura', '(UTC+05:45) Kathmandu', '(UTC+06:00) Astana, Dhaka', '(UTC+06:00) Almaty, Novosibirsk', '(UTC+06:30) Yangon Rangoon', '(UTC+07:00) Bangkok, Hanoi, Jakarta', '(UTC+07:00) Krasnoyarsk', '(UTC+08:00) Beijing, Chongqing, Hong Kong SAR, Urumqi', '(UTC+08:00) Kuala Lumpur, Singapore', '(UTC+08:00) Taipei', '(UTC+08:00) Western Australia', '(UTC+08:00) Irkutsk, Ulaanbaatar', '(UTC+08:45) Western Australia (Eucla)', '(UTC+09:00) Seoul', '(UTC+09:00) Osaka, Sapporo, Tokyo', '(UTC+09:00) Yakutsk', '(UTC+09:30) Darwin', '(UTC+09:30) Adelaide', '(UTC+10:00) Canberra, Melbourne, Sydney', '(UTC+10:00) Brisbane', '(UTC+10:00) Hobart', '(UTC+10:00) Vladivostok', '(UTC+10:00) Guam, Port Moresby', '(UTC+10:30) Lord Howe', '(UTC+11:00) Magadan, Solomon Islands, New Caledonia', '(UTC+11:00) Norfolk Island', '(UTC+12:00) Fiji Islands, Kamchatka, Marshall Islands', '(UTC+12:00) Auckland, Wellington', '(UTC+12:00) Tarawa', '(UTC+12:45) Chatham Islands', "(UTC+13:00) Nuku'alofa", '(UTC+14:00) Kiritimati'].freeze

  has_many :event_users
  has_many :users, through: :event_users
  belongs_to :user
  has_one_attached :cover_image
  accepts_nested_attributes_for :event_users

  scope :past, -> { where('end_date_time < ?', DateTime.now) }

  def speakers
    event_users.where('speaker = ?', true)
  end

  def get_date
    if start_date == end_date
      "#{start_date}, #{start_time} - #{end_time}"
    else
      "#{start_date}, #{start_time} - #{end_date}, #{end_time}"
    end
  end

  def start_date
    start_date_time.strftime('%a, %e %b %Y')
  end

  def end_date
    end_date_time.strftime('%a, %e %b %Y')
  end

  def start_time
    start_date_time.strftime('%l:%M %p')
  end

  def end_time
    end_date_time.strftime('%l:%M %p')
  end
end
