class Group < ApplicationRecord
    validates :name, :description, presence: :true

    has_many :user_groups
    has_many :users, through: :user_groups
    belongs_to :creator, class_name: 'User', foreign_key: :user_id

    # scope :accepted, -> { joins(:user_groups).where('user_group.status=?', 1) }
end


# t.string "name"
# t.string "description"
# t.integer "user_id"
# t.boolean "visible"
# t.boolean "can_invite_connections"