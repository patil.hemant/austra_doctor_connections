class NonHcp < ApplicationRecord
	acts_as :user

	# attr_accessor :mobile_no, :govt_id_proof, :country_id

	# validates :mobile_no, presence: :true
	
	has_one_attached :organization_id
end


#only single field is being stored in this table. organization attachment. other fields are inheriting from user table. cross check later