class Post < ApplicationRecord
	attr_accessor :posting_as

	self.per_page = 10
	validates :title, presence: :true, if: -> { expert_opinion? }

	enum post_type: {
		normal_post: 0,
		question: 1,
		expert_opinion: 2,
		article: 3
	}

	has_rich_text :description

	has_many_attached :files
	has_one_attached :document #cover picture for article
	has_one_attached :clip
	belongs_to :user, optional: :true
	belongs_to :business, optional: :true

	has_many :comments, as: :commentable
	has_many :likes, as: :likeable
	has_many :polls
	accepts_nested_attributes_for :polls, :reject_if => lambda { |a| a[:question].blank? }, :allow_destroy => true

	def normal_post?
		post_type.include?('normal_post')
	end

	def question?
		post_type.include?('question')
	end

	def expert_opinion?
		post_type.include?('expert_opinion')
	end

	def article?
		post_type.include?('article')
	end

	def poll?
		post_type.include?('normal_post') && polls.present?
	end
end
