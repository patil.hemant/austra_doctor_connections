class Resume < ApplicationRecord
	has_one_attached :file

	validates :file, presence: true
	belongs_to :user
end
