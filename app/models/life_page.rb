class LifePage < ApplicationRecord
    belongs_to :business
    has_many :spotlights

    has_one_attached :image_or_video

    accepts_nested_attributes_for :spotlights
end
