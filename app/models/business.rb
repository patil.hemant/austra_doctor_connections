class Business < ApplicationRecord
  has_one_attached :logo
  has_many :courses
  has_many :life_pages
  has_many :leaders
  has_many :users
  has_many :users, through: :businesses
  has_many :posts

  accepts_nested_attributes_for :life_pages

  def college
    #convert business_type to ENUM later
    business_type.include?('Medical College')
  end

  def medical_association #cant use just association. as ruby reserverd keyword.
    #convert business_type to ENUM later
    business_type.include?('Medical Association')
  end

  def hospital
    #convert business_type to ENUM later
    business_type.include?('Hospital')
  end

  def clinic
    #convert business_type to ENUM later
    business_type.include?('Clinic')
  end
end
