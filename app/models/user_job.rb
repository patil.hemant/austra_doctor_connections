class UserJob < ApplicationRecord
	belongs_to :user
	belongs_to :applied_job, class_name: Job.to_s, foreign_key: :job_id

	validates :user_id, :uniqueness => {:scope => :job_id}
end