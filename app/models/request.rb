class Request < ApplicationRecord
	belongs_to :requestor, class_name: :User
  belongs_to :receiver, class_name: :User

  def self.request_connection(params)
	  Request.create(params)
	end
end
