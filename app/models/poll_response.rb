class PollResponse < ApplicationRecord
	validates :poll, uniqueness: { scope: :user } 

    belongs_to :poll_answer
    belongs_to :poll
    belongs_to :user

    after_save :calculate_percentage_vote
	after_destroy :calculate_percentage_vote
	after_create :add_votes_count
	after_destroy :reduce_votes_count

	def calculate_percentage_vote
		poll.poll_answers.includes(:poll_responses).each do |poll_answer|
			poll_responses = poll.poll_responses
			unless poll_responses.present?
				percentage = 0
			else
				answer_responses = poll_answer.poll_responses
				percentage = ((answer_responses.count.to_f / poll_responses.count.to_f) * 100).round
			end
			poll_answer.update_column('vote_percentage', percentage)
		end
	end

	def add_votes_count
		poll.update_column('votes_count', poll.votes_count.to_i + 1)
	end

	def reduce_votes_count
		poll.update_column('votes_count', poll.votes_count.to_i - 1)
	end
end
