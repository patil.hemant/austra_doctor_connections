class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  actable

  attr_accessor :speciality_id, :domain_id, :username, :current_step, :role, :about, :organization_id

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :connection_requests_as_requestor,
    foreign_key: :requestor_id,
    class_name: :Request

  has_many :connection_requests_as_receiver, 
    foreign_key: :receiver_id,
    class_name: :Request

  has_many :a_users, foreign_key: :user_a_id, class_name: :Connection
  has_many :b_users, foreign_key: :user_b_id, class_name: :Connection
  has_many :owned_groups, class_name: 'Group'
  has_many :created_jobs, class_name: 'Job'
  has_many :posts
  has_many :poll_responses
  has_many :business_users
  has_many :businesses, through: :business_users

  has_many :user_groups
  has_many :groups, through: :user_groups

  has_many :user_jobs
  has_many :applied_jobs, through: :user_jobs, class_name: Job.to_s

  has_many :user_saved_jobs
  has_many :saved_jobs, through: :user_saved_jobs, class_name: Job.to_s

  has_one_attached :govt_id_proof
  has_many :resumes

  has_many :event_users
  has_many :events, through: :event_users

  validates :mobile_no, presence: :true, if: -> { current_step?('step_two') }

  before_validation :set_email, :if => Proc.new{ |u| username.present? && domain_id.present? }

  def current_step?(step_key)
    current_step == step_key
  end

  def incoming_pending_requests
    User.where(id: connection_requests_as_receiver.map{|request|request.requestor_id})
  end

  def outgoing_pending_requests
    User.where(id: connection_requests_as_receiver.map{|request|request.receiver_id})
  end

  def connected_users
    connections = Connection.where("user_a_id = ? OR user_b_id = ?", id, id)
    User.where(id: connections.map{|c| c.user_a_id != id ? c.user_a_id : c.user_b_id})
  end

  def name
    [first_name, last_name].compact.join(' ').titleize
  end

  def name_with_email
    [first_name, last_name, " (#{email})"].compact.join(' ').titleize
  end

  private

  def set_email
    domain = DomainName.find(domain_id)
    self.email = "#{username}@#{domain.name}"
  end
end
