class Course < ApplicationRecord
	belongs_to :business
	belongs_to :category

	validates :title, :description, presence: :true
end