class LikesController < ApplicationController
  before_action :find_likeable, only: [:create]
  
  def create
    @likeable.likes.create(user_id: current_user.id)
  end

  def destroy
  	@like = Like.find(params[:id])
  	@likeable = @like.likeable
  	@like.destroy
  end
  
  private
  def find_likeable
    @likeable = params[:likeable_type].constantize.find(params[:post_id]) if params[:likeable_type].include?('Post')
    @likeable = params[:likeable_type].constantize.find(params[:comment_id]) if params[:likeable_type].include?('Comment')
  end
end