class GroupsController < ApplicationController
	def new
		@group = Group.new()
	end

	def create
		@group = Group.new(group_params)
		@group.user_id = current_user.id
		UserGroup.create(user_id: current_user.id, group_id: @group.id, status: 1)	if @group.save
		# if @group.save
		# 	redirect_to groups_path
		# else
		# 	render :new
		# end
		respond_to do |format|
			format.js
		end
	end

	def index
		@group = Group.new()
		@groups = current_user.user_groups.where('user_groups.status = ?', 1)
		@requested_groups = current_user.user_groups.where('status = ?', 0)
		@requests_groups = UserGroup.where('group_id IN (?) and status = ?', current_user.owned_groups.pluck(:id), 0).includes(:group)
		@owned_groups = current_user.owned_groups.pluck(:id)
	end

	def show
		@group = Group.find(params[:id])
		@users = User.all
	end

	def destroy
		@group.destroy
		respond_to do |format|
			format.js
		end
	end
	

	private

	def group_params
		params.require(:group).permit!
	end
end
