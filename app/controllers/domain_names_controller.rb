class DomainNamesController < ApplicationController
  def index
		@speciality = Speciality.find(params[:id])
		@domains = @speciality.domain_names
	end
end
