class ConnectionsController < ApplicationController
	def index
		@connections_active = 'active'
		@people_you_may_know = people_you_may_know
		@incoming_pending_requests = current_user.incoming_pending_requests
		user_ids = UserGroup.select(:user_id)
		group_ids = UserGroup.where('user_id =?', current_user.id).select(:group_id)
		@groups = Group.where('id NOT IN (?)', group_ids)
		@groups_count = current_user.user_groups.where('user_groups.status = ?', 1).count
	end

	def create
		@connection = Connection.create(connection_params)
		destroy_request
	end

	def destroy
		@connection = current_user.a_users.where('user_b_id = ?', params[:id]).last

  	if @connection.blank?
  		@connection = current_user.b_users.where('user_a_id = ?', params[:id]).last
  	end

  	@connection.destroy
	end

	def my_connections
		@connections = current_user.connected_users
	end
end

private

	def connection_params
		params.require(:connection).permit(:user_a_id, :user_b_id)
	end

	def people_you_may_know
		User.all.where.not(id: current_user.id)
	end

	def destroy_request
		request = current_user.connection_requests_as_receiver.where(requestor_id: @connection.user_a_id).last
		request.destroy
	end