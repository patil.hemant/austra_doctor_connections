class RequestsController < ApplicationController
	def create
		@connection_req = Request.request_connection(request_params)
	end

	def destroy
		@request = current_user.connection_requests_as_receiver.where(requestor_id: params[:id]).last
		@request.destroy
	end

	private

	def request_params
		params.require(:request).permit(:requestor_id, :receiver_id)
	end
end
