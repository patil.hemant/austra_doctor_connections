class PostsController < ApplicationController
	def create
		@post = Post.new(post_params)
		@post.user_id= current_user.id
		@post.save
	end

	private

	def post_params
		params.require(:post).permit(:business_id, :title, :description, :document, :clip, :post_type, files: [], polls_attributes: [:id, :_destroy, :question, :duration, poll_answers_attributes: [:answer, :_destroy]])
	end
end
