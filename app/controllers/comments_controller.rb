class CommentsController < ApplicationController

  before_action :find_commentable

    def new
      @comment = Comment.new
    end

    def create
      @comment = @commentable.comments.new(comment_params)
			@comment.save
    end

    private

    def comment_params
      params.require(:comment).permit(:content, :commentable_type, :commentable_id, :parent_id)
    end

    def find_commentable
      @commentable = comment_params[:commentable_type].constantize.find_by_id(comment_params[:commentable_id])
    end
  end