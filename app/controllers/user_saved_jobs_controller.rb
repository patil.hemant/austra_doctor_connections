class UserSavedJobsController < ApplicationController
	before_action :sanitize_page_params, only: [:update]
	before_action :set_user_saved_job, only: [:update]

	def create
		@user_saved_job = UserSavedJob.new(user_saved_job_params)
		@user_saved_job.save
		respond_to do |format|
			format.js
		end
	end

	def destroy
		@user_saved_job = current_user.user_saved_jobs.where(job_id: params[:job_id]).last
		@user_saved_job.destroy
		respond_to do |format|
			format.js { render 'destroy' } 
		end
	end

	def update
		@user_saved_job.update_attributes(user_saved_job_params)
		respond_to do |format|
			format.js
		end
	end

private

	def set_user_saved_job
		@user_saved_job = UserSavedJob.find(params[:id])
		@id = @user_saved_job.id
	end

	def sanitize_page_params
		params[:user_saved_job] = {"status" => user_saved_job_params['status'].to_i}
	end

	def user_saved_job_params
		params.require(:user_saved_job).permit(:user_id, :job_id, :status)
	end
end
