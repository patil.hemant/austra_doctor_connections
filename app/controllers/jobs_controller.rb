class JobsController < ApplicationController
	require 'action_view'
	require 'action_view/helpers'
	include ActionView::Helpers::DateHelper

	before_action :set_job_counts, only: [:index, :my_applied_jobs, :saved_jobs, :my_posted_jobs]
	before_action :set_active, only: [:index, :show, :my_applied_jobs, :saved_jobs, :my_posted_jobs]
	before_action :initiate_job, only: [:index, :show, :new, :my_applied_jobs, :saved_jobs, :my_posted_jobs]

	def index
		@job = Job.new
		applied_jobs = current_user.applied_jobs.pluck(:id)
		saved_jobs = current_user.saved_jobs.pluck(:id)
		created_jobs = current_user.created_jobs.pluck(:id)
		job_ids = applied_jobs + saved_jobs + created_jobs

		@jobs = Job.where('user_id NOT IN (?) and id NOT IN (?)', [current_user.id], job_ids+[0])
	end

	def new
		@job = Job.new()
	end

	def create
		@job = Job.new(job_params)
		@job.user_id = current_user.id
		
		# if @job.save
		# 	redirect_to jobs_url
		# else
		# 	render 'new'
		# end
		@job.save
		respond_to do |format|
			format.js
		end
	end

	def show
		@objekt = Job.find(params[:id])
		@job = Job.new()
	end
	
	def my_applied_jobs
		# @jobs = current_user.user_jobs
		@jobs = current_user.applied_jobs
	end

	def saved_jobs
		@jobs = current_user.saved_jobs
	end	

	def my_posted_jobs
		@jobs = current_user.created_jobs
	end

	def states
		states = CS.states(params[:country].to_sym)
		render json: states
	end

	def cities
		cities = CS.cities(params[:state].to_sym, params[:country].to_sym)
		keys = cities
		render json: Hash[keys.zip(cities)]
	end

	private

	def set_job_counts
		@created_jobs_count = current_user.created_jobs.count
		@applied_jobs_count = current_user.applied_jobs.count
		@saved_jobs_count = current_user.saved_jobs.count
	end

	def job_params
		params.require(:job).permit!
	end

	def set_active
		@jobs_active = 'active'
	end

	def initiate_job
		@job = Job.new
	end
	
end
