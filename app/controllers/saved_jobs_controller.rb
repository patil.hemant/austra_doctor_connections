class SavedJobsController < ApplicationController
	before_action :sanitize_page_params, only: [:update]
	before_action :set_saved_job, only: [:update, :destroy]

	def create
		@saved_job = SavedJob.new(saved_job_params)
		@saved_job.save
		respond_to do |format|
				format.js
		end
	end

	def destroy
		@saved_job.destroy
		respond_to do |format|
			format.js { render 'update' } 
		end
	end

	def update
		@saved_job.update_attributes(saved_job_params)
		respond_to do |format|
				format.js
		end
	end

private

	def set_saved_job
		@saved_job = SavedJob.find(params[:id])
		@id = @saved_job.id
	end

	def sanitize_page_params
		params[:saved_job] = {"status" => saved_job_params['status'].to_i}
	end

	def saved_job_params
		params.require(:saved_job).permit(:user_id, :job_id)
	end
end
