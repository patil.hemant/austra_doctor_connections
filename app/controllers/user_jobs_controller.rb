class UserJobsController < ApplicationController
	before_action :sanitize_page_params, only: [:update]
	before_action :set_user_job, only: [:update, :destroy]

	def create
		@user_job = UserJob.new(user_job_params)
		@user_job.save
		
		respond_to do |format|
				format.js
		end
	end

	def destroy
		@user_job.destroy
		respond_to do |format|
			format.js { render 'update' } 
		end
	end

	def update
		@user_job.update_attributes(user_job_params)
		respond_to do |format|
				format.js
		end
	end

private

	def set_user_job
		@user_job = UserJob.find(params[:id])
		@id = @user_job.id
	end

	def sanitize_page_params
		params[:user_job] = {"status" => user_job_params['status'].to_i}
	end

	def user_job_params
		params.require(:user_job).permit(:user_id, :job_id, :status)
	end
end
