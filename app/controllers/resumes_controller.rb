class ResumesController < ApplicationController
	def create
		@resume = Resume.new(resume_params)
		@resume.user_id = current_user.id
		@resume.save
		
		respond_to do |format|
			format.js
		end
	end

	def index
		@resumes = current_user.resumes
		@resume = Resume.new
	end

	private

	def resume_params
		params.require(:resume).permit!
	end
end
