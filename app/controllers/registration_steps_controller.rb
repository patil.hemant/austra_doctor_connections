class RegistrationStepsController < ApplicationController
  include Wicked::Wizard
  
  steps :step_two
  
  def show
    @user = current_user.specific
    @user_role = @user.class.name.downcase
    return redirect_to root_path if %w[other].include?(@user_role)

    render_wizard
  end
  
  def update
    @user = current_user.specific
    @user.attributes = user_params
    # binding.pry
    if @user.valid?
      set_user_attributes
    end
    render_wizard @user
  end
  
  private

  def set_user_attributes
    user = @user.acting_as
    user.mobile_no = @user.mobile_no
    user.country_id = @user.country_id
    user.govt_id_proof = @user.govt_id_proof.blob if user.govt_id_proof.attached?
    user.save
  end

  def user_params
    @user_role = @user.class.name.downcase

    if @user_role.include?('doctor')
      params.require(:doctor).permit!
    elsif @user_role.include?('hcp')
      params.require(:hcp).permit!
    elsif @user_role.include?('student')
      params.require(:student).permit!
    end
  end

  def redirect_to_finish_wizard(options = nil, params = {})
    redirect_to root_path , notice: "Thank you for signing up."
  end
end
