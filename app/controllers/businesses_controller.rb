class BusinessesController < ApplicationController
    def new
			@business = Business.new
    end

    def create
			@business = Business.new(business_params)
			create_business_user_record if @business.save
			
			respond_to do |format|
				format.html{ redirect_to business_path(@business) }
			end
    end

		def show
			@business = Business.find(params[:id])
			@course = @business.courses.new
			@courses = @business.courses.group(:category_id)
			life_page = @business.life_pages.build
			life_page.spotlights.build
			@event = Event.new
			@event_users = @event.event_users.build
			@posts = @business.posts.paginate(page: params[:page], per_page: 15).order('created_at DESC')

			courses

			@category = Category.new
		end

    private

    def business_params
      params.require(:business).permit!
    end

		def courses
			sql = "select categories.name, title, description from courses inner join categories on categories.id = courses.category_id"
			result = ActiveRecord::Base.connection.execute(sql)
			@courses = result.entries.group_by { |h| h['name'] }
		end

		def create_business_user_record
			current_user.business_users.create(
				business_id: @business.id
			)
		end
end
