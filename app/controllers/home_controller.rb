class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
    @home_active = 'active'
    @posts = Post.includes(:polls).paginate(page: params[:page], per_page: 15).order('created_at DESC')
    @event = Event.new
    @groups = current_user.user_groups.where('user_groups.status = ?', 1)
    @event_users = @event.event_users.build
    @post = current_user&.posts&.new || Post.new
    @article = current_user&.posts&.new || Post.new
    @businesses = current_user.businesses.select(:id, :name)

    poll = @post.polls.build
    2.times { poll.poll_answers.build }
    
    respond_to do |format|
      format.html
      format.js
    end
  end
end
