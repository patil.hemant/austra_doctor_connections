class UserGroupsController < ApplicationController
		before_action :sanitize_page_params, only: [:update]
		before_action :set_user_group, only: [:update, :destroy]

    def create
			@user_group = UserGroup.new(user_group_params)
			@user_group.save
			respond_to do |format|
				format.js
			end
    end

		def destroy
			@user_group.destroy
			respond_to do |format|
				format.js { render 'update' } 
			end
		end

		def update
			@user_group.update_attributes(user_group_params)
			respond_to do |format|
				format.js
			end
		end

    private

		def set_user_group
			@user_group = UserGroup.find(params[:id])
			@id = @user_group.id
		end

		def sanitize_page_params
			params[:user_group] = {"status" => user_group_params['status'].to_i}
		end

		def user_group_params
			params.require(:user_group).permit(:user_id, :group_id, :status)
		end
end
