class CoursesController < ApplicationController
	# require 'action_view'
	# require 'action_view/helpers'
	# include ActionView::Helpers::DateHelper

	# before_action :set_job_counts, only: [:index, :my_applied_jobs, :saved_jobs, :my_posted_jobs]
	# before_action :set_active, only: [:index, :show, :my_applied_jobs, :saved_jobs, :my_posted_jobs]
	# before_action :initiate_job, only: [:index, :show, :new, :my_applied_jobs, :saved_jobs, :my_posted_jobs]

	def index
		@course = Job.new
		applied_jobs = current_user.applied_jobs.pluck(:id)
		saved_jobs = current_user.saved_jobs.pluck(:id)
		created_jobs = current_user.created_jobs.pluck(:id)
		job_ids = applied_jobs + saved_jobs + created_jobs

		@courses = Job.where('user_id NOT IN (?) and id NOT IN (?)', [current_user.id], job_ids+[0])
	end

	def new
		@course = Job.new()
	end

	def create
		@course = Course.new(course_params)
		
		# if @course.save
		# 	redirect_to jobs_url
		# else
		# 	render 'new'
		# end
		@business_id = @course.business_id
		@course.save
		courses
		respond_to do |format|
			format.js
		end
	end

	def show
		@objekt = Job.find(params[:id])
		@course = Job.new()
	end
	
	def my_applied_jobs
		# @courses = current_user.user_jobs
		@courses = current_user.applied_jobs
	end

	def saved_jobs
		@courses = current_user.saved_jobs
	end	

	def my_posted_jobs
		@courses = current_user.created_jobs
	end

	def states
		states = CS.states(params[:country].to_sym)
		render json: states
	end

	def cities
		cities = CS.cities(params[:state].to_sym, params[:country].to_sym)
		keys = cities
		render json: Hash[keys.zip(cities)]
	end

	private

	def courses
		sql = "select categories.name, title, description from courses inner join categories on categories.id = courses.category_id"
		result = ActiveRecord::Base.connection.execute(sql)
		@courses = result.entries.group_by { |h| h['name'] }
	end

	def set_job_counts
		@created_jobs_count = current_user.created_jobs.count
		@applied_jobs_count = current_user.applied_jobs.count
		@saved_jobs_count = current_user.saved_jobs.count
	end

	def course_params
		params.require(:course).permit!
	end

	def set_active
		@courses_active = 'active'
	end

	def initiate_job
		@course = Job.new
	end
	
end
