class CategoriesController < ApplicationController
    def create
			@category = Category.new(category_params)
			@category.save
			
			respond_to do |format|
					format.js
			end
    end

    private

    def category_params
			params.require(:category).permit!
    end
end
