class PollResponsesController < ApplicationController
    def create
      @poll_response = PollResponse.new(poll_response_params)
			@poll_response.save
      @poll = @poll_response.poll

			respond_to do |format|
				format.js
				format.html
			end
    end

    def destroy
      @poll_response = PollResponse.find(params[:id])
      @poll_response.destroy
      @poll = @poll_response.poll


      respond_to do |format|
				format.js
				format.html
			end
    end

    private

    def poll_response_params
      params.require(:poll_response).permit!
    end
end
