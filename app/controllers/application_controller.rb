class ApplicationController < ActionController::Base
	before_action :configure_permitted_parameters, if: :devise_controller?

	protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:role, :first_name, :last_name, :speciality_id, :domain_id, :username, :signed_up_as_admin, :about, :mobile_no, :country_id, :terms_and_conditions, :govt_id_proof, :organization_id])
  end
end
