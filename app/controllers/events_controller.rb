class EventsController < ApplicationController
  def create
    @event = Event.new(event_params)
    @event.user_id = current_user.id
    speaker_ids = event_params[:event_users_attributes]['0']['user_id'].reject{|e|e.blank?}
    @event.event_users.delete_all

    speaker_ids.each do |user_id|
      @event.event_users.new(user_id: user_id, speaker: true)
    end

    @event.save
		
    respond_to do |format|
      format.js
    end
  end

  def show
    @event = Event.find(params[:id])
    @speakers = @event.speakers
  end

  def index
    @past_events = current_user.events.past
    @events = current_user.events
  end

  def get_users
    @users = User
            .where("users.email ilike ? OR users.first_name ilike ? OR users.last_name ilike ?", "%#{params['search_text']['term']}%", "%#{params['search_text']['term']}%","%#{params['search_text']['term']}%")
            .select('users.id, users.email, users.first_name, users.last_name')
    
    # @users = User.joins(:profile)
    #              .select('users.sofu_uniq_code, users.email, users.first_name, users.last_name')
    respond_to do |format|
      format.json { render json: @users }
    end
    # respond_with @users
  end

  private

  def event_params
    params.require(:event).permit(:cover_image, :organizable_id, :event_type, :name, :timezone, :start_date_time, :end_date_time, :link, :description, :user_id, event_users_attributes: [:user_id => []])
  end
end
