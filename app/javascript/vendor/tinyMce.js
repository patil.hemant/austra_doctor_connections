import tinymce from 'tinymce/tinymce';
import 'tinymce/themes/silver/theme';
import 'tinymce/plugins/table';
import 'tinymce/plugins/lists';
import 'tinymce/icons/default';
import 'tinymce/models/dom';
import 'tinymce/skins/ui/oxide/skin.min.css';
import 'tinymce/skins/ui/oxide/content.min.css';
import 'tinymce/skins/content/default/content.css';

function tinyMce() { 
    tinymce.init({ 
        selector: 'textarea.tinymce', 
        skin: false,
        width : '100%',
        height: 300
    });
}
export { tinyMce };