jQuery(function() {
    if ($('#infinite-scrolling').length > 0) {
      $(window).on('scroll', function() {
        var more_posts_url;
        more_posts_url = $('.pagination .next_page a').attr('href');
        if (more_posts_url && $(window).scrollTop() > $(document).height() - $(window).height() - 30) {
            $('.pagination').html('<img src="/assets/ajax-loader.gif" alt="Loading..." title="Loading..." />')

        //   $('.pagination').html('<div id="infinite-spinner">loading <i class="fas fa-spinner fa-spin"></i></div>');
          $.getScript(window.location.origin + more_posts_url);
        }
      });
    }
});
  