// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("@rails/activestorage").start()
require("channels")

require("@popperjs/core")
import "jquery"

// Import the specific modules you may need (Modal, Alert, etc)
import { Tooltip, Popover } from "bootstrap"
import "@fortawesome/fontawesome-free/css/all"
import { tinyMce } from "../vendor/tinyMce";
import { flatpickr } from "flatpickr";
require('flatpickr/dist/flatpickr.css')

global.toastr = require("toastr")

// The stylesheet location we created earlier
require("../stylesheets/application.scss")

require("custom/select2.full")
require("packs/will_paginate")
require("packs/lightbox")
import "stylesheets/select2.min"
import "cocoon";

window.jQuery = $;
window.$ = $;

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

$(document).ready(function(){

  tinyMce();
	$('.start-a-post').click(function(){
		$('.post-modal').trigger('click')
    $('#startPostModal').modal('show')
	})

	$('.ask-a-question').click(function(){
    $('#askQuestionModal').modal('show')
	})

	$('.get-expert-opinion').click(function(){
    $('#expertOpinionModal').modal('show')
	})

  $('.write-an-article').click(function(){
    $('#articleModal').modal('show')
	})


})

$(document).on('click', '.uploadDoc', function(){
  $(this).closest('form').find('.docUpload').trigger('click')
})

$(document).on('click', '.uploadFile', function(){
  $(this).closest('form').find('.fileUpload').trigger('click')
})

$(document).on('click', '.uploadVideo', function(){
  $(this).closest('form').find('.clipUpload').trigger('click')
})

$(document).on('click', '.reply-comment', function(){
  $(this).closest('.comments-wrapper').find('.reply-form').removeClass('d-none')
})

$(document).on('click', '.repy-to-reply', function(){
  $(this).closest('.comments-wrapper').find('.reply-comment').trigger('click')
})

// polls post
$(document).on('click', '.view-poll-results', function(){
  var thisRef = $(this).closest('.poll-section')
  thisRef.find('.hide-poll-results').show()
  thisRef.find('.poll-result-wrapper').removeClass('hide')
  thisRef.find('.submit-poll-response').hide()
  $(this).hide()
})

$(document).on('click', '.hide-poll-results', function(){
  var thisRef = $(this).closest('.poll-section')
  thisRef.find('.view-poll-results').show()
  thisRef.find('.poll-result-wrapper').addClass('hide')
  thisRef.find('.submit-poll-response').show()
  $(this).hide()
})

// polls post


$(document).ready(function() {
  // $("#fileUpload").on('change', function() {
  //   //Get count of selected files
  //   var countFiles = $(this)[0].files.length;
  //   var imgPath = $(this)[0].value;
  //   var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
  //   var image_holder = $("#image-holder");
  //   image_holder.empty();
  //   if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
  //     if (typeof(FileReader) != "undefined") {
  //       //loop for each file selected for uploaded.
  //       for (var i = 0; i < countFiles; i++) 
  //       {
  //         var reader = new FileReader();
  //         reader.onload = function(e) {
  //           $("<img />", {
  //             "src": e.target.result,
  //             "class": "img-thumbnail"
  //           }).appendTo(image_holder);
  //         }
  //         image_holder.show();
  //         reader.readAsDataURL($(this)[0].files[i]);
  //       }
  //     } else {
  //       alert("This browser does not support FileReader.");
  //     }
  //   } else {
  //     alert("Pls select only images");
  //   }
  // });

  var imgUpload = $('#fileUpload')
  , imgPreview = $('#image-holder')
  , imgUploadForm = $('#startPost')
  , totalFiles
  , previewTitle
  , previewTitleText
  , img;

  $("#fileUpload").on('change', function(event) {
    totalFiles = imgUpload[0].files.length;
    
      if(!!totalFiles) {
        imgPreview[0].classList.remove('img-thumbs-hidden');
      }
    
    for(var i = 0; i < totalFiles; i++) {
      var wrapper = document.createElement('div');
      wrapper.classList.add('wrapper-thumb');
      var removeBtn = document.createElement("span");
      var nodeRemove= document.createTextNode('x');
      removeBtn.classList.add('remove-btn');
      removeBtn.appendChild(nodeRemove);
      var img = document.createElement('img');
      img.src = URL.createObjectURL(event.target.files[i]);
      img.classList.add('image-holder');
      img.classList.add('img-preview-thumb');
      wrapper.appendChild(img);
      wrapper.appendChild(removeBtn);
      imgPreview[0].appendChild(wrapper);
    
      $('.remove-btn').click(function(){
        $(this).parent('.wrapper-thumb').remove();
      });    
    }
  });

  var imgUpload = $('#expertOpinionFileUpload')
  , imgPreview = $('#expert-opinion-image-holder')
  , imgUploadForm = $('#expertOpinionForm')
  , totalFiles
  , previewTitle
  , previewTitleText
  , img;

  $("#expertOpinionFileUpload").on('change', function(event) {
    totalFiles = imgUpload[0].files.length;
    
      if(!!totalFiles) {
        imgPreview[0].classList.remove('img-thumbs-hidden');
      }
    
    for(var i = 0; i < totalFiles; i++) {
      var wrapper = document.createElement('div');
      wrapper.classList.add('wrapper-thumb');
      var removeBtn = document.createElement("span");
      var nodeRemove= document.createTextNode('x');
      removeBtn.classList.add('remove-btn');
      removeBtn.appendChild(nodeRemove);
      var img = document.createElement('img');
      img.src = URL.createObjectURL(event.target.files[i]);
      img.classList.add('image-holder');
      img.classList.add('img-preview-thumb');
      wrapper.appendChild(img);
      wrapper.appendChild(removeBtn);
      imgPreview[0].appendChild(wrapper);
    
      $('.remove-btn').click(function(){
        $(this).parent('.wrapper-thumb').remove();
      });    
    }
  });

});

require("trix")
require("@rails/actiontext")