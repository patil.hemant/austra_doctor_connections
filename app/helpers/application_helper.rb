module ApplicationHelper
	def govt_id_label(user_role)
		if %w[doctor hcp].include?(user_role)
			'Upload your Govt. ID with photo (Front and Back)'
		elsif user_role.include?('student')
			'Upload your PHOTO ID (Govt ID or College, University ID)'
		end	
	end	
end
