module PollResponsesHelper
    def vote_record(poll)
			poll.poll_responses.where('user_id = ?', current_user&.id).last
    end
end
