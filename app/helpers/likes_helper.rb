module LikesHelper
	def like_record likeable
    	likeable.likes.where(user_id: current_user&.id).last
  	end
end
