module PostsHelper
    def post_image_tag(file)
        if file.variable?
            link_to (image_tag file.variant(resize: "400x400")), file, :data=>{:lightbox=>"post-image-#{file.record_id}"}
        elsif file.previewable?
            link_to (image_tag file.preview(resize: "400x400"), rails_blob_path(file, disposition: :attachment)), rails_blob_path(file, disposition: :attachment), :data=>{:lightbox=>"post-image-#{file.record_id}"}
        elsif file.previewable?
        else
            link_to file.filename, rails_blob_path(file, disposition: :attachment) 
        end
    end
end
